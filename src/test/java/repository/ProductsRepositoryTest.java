package repository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.demoWeb.DemoWebApplication;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes=DemoWebApplication.class)
@Transactional
public class ProductsRepositoryTest {

	@Autowired
	private ProductsRepository productsRepository;

	@Test
	public void testFindAll_andProducerWorked() {
		assertThat(productsRepository.findAll()).hasSize(1);
	}

	@Test
	public void findByNameAndPrice_andNoneFound() {
		assertThat(productsRepository.FindByNameAndPrice("Hello World", 10)).isNull();
	}

	@Test
	public void findByNameAndPrice_andFoundOne() {
		assertThat(productsRepository.FindByNameAndPrice("Overwatch Ashe Funko Shirt", 20.00)).isNotNull();
	}

}
