package entities;

public class Products {

    private String description;
    private String name;
    private String image;
    private double price;
    private int uid;
	
    public Products(String description, String name, double price, int uid) {
		this.description = description;
		this.name = name;
		this.price = price;
		this.uid = uid;
	}
	
    public Products(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public Products() {
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String toString() {
		return "Products [description=" + description + ", name=" + name + ", image=" + image + ", price=" + price
				+ ", uid=" + uid + "]";
	}

}
