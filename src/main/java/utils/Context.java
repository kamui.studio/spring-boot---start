package utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Context {

	private EntityManagerFactory EMF = null;

    private static Context ctx = null;

    private Context() {
        EMF = Persistence.createEntityManagerFactory("jpa-tests");
    }

    public static Context getInstance() {
        if (ctx == null) {
            ctx = new Context();
        }
        return ctx;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return EMF;
    }

    public static void destroy() {
        if (ctx != null) {
            ctx.EMF.close();
            ctx = null;
        }
    }

}
