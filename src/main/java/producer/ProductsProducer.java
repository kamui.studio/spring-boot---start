package producer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entities.Products;
import service.ProductsService;

@Component
public class ProductsProducer {

	private Log logger = LogFactory.getLog(ProductsProducer.class);
	
	private ProductsService productsService;
	
	@Autowired
	public ProductsProducer(ProductsService productsService) {
		this.productsService = productsService;
	}
	
	@PostConstruct
	public void produceDatas() {
		findProducts();
		addOneProduct();
		findProducts();
	}
	
	private void addOneProduct() {
		logger.info("→ Adding a new product now!");
		productsService.addProduct(new Products("T-shirt Bob Overwatch - Homme", 25.00));
	}

	private void findProducts() {
		logger.info("Trying to find all products.");
		List<Products> allProducts = productsService.getAllProducts();
		if (allProducts.isEmpty())
			logger.info("-- No items found --");
		else
			for (Products products : allProducts)
				logger.info(String.format("Product with id %d and name %s found.", products.getUid(), products.getName()));
	}
	
}
