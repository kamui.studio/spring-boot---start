package service;

import entities.Products;
import repository.ProductsRepository;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductsService {

	@Autowired
	private ProductsRepository productsRepository;
	
	@Transactional(readOnly = true)
	public List<Products> getAllProducts() {
		return productsRepository.findAll();
	}

	public void addProduct(Products product) {
		productsRepository.save(product);
	}

	public void deleteProduct(Integer id) {
		productsRepository.deleteById(id);
	}
	
}
