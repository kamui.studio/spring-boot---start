package repository;

import entities.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;


public interface ProductsRepository extends JpaRepository<Products, Integer> {

	Products FindByNameAndPrice (
		@Param("name") String name,
		@Param("price") double price
	);
	
}
