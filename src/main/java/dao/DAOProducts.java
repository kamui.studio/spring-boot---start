package dao;

import java.util.*;
import javax.persistence.*;

import entities.Products;
import utils.Context;

public class DAOProducts implements DAO<Products, Integer> {

	
	public void create(Products p) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.persist(p);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

	public Products findById(Integer uid) {
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

		Products product = EM.find(Products.class, uid);

        EM.close();
        Context.destroy();
		return product;
		
	}
	
	public List<Products> findAll(){
		
		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();

        Query q = EM.createQuery("SELECT p FROM Products p");
        List<Products> products = q.getResultList();

        EM.close();
        Context.destroy();
		return products;
		
	}
	
	public void update(Products p) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.merge(p);

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}
	
	public void delete(Products p) {

		EntityManager EM = Context.getInstance().getEntityManagerFactory().createEntityManager();
        EM.getTransaction().begin();

        EM.remove(EM.merge(p));

        EM.getTransaction().commit();
        EM.close();
        Context.destroy();
		
	}

}